"""This program is for reproducing the results of Perna, R., Bozzo, E., & Stella, L. 2006, ApJ, 639, 363.
Figure 9 of Perna et al. 2006:
    For each time step, a plot is created including the frequency derivative over the total mass accretion rate
    and the mass accretion rate from the star over the total mass accretion rate.
Figures 10 and 11 of Perna et al. 2006 can be produced by adjusting the parameters as described in the paper, including:
    - Frequency over time
    - Frequency derivative over time
    - Luminosity over time
"""
import matplotlib.pyplot as plt
import numpy as np
import quantities as pq

"""
---------------------
Constants
---------------------
G is the gravitational constant in SI units.
G_cgs is the gravitational constant in cgs units.
SOLARMASS is the mass of the sun in kg.
M_NS is the mass of a typical Neutron Star in gram.
R_NS is the radius of a typical Neutron Star in cm.
I_NS is the moment of inertia of the Neutron Star in units of g*cm**2 (Revnivtsev, p. 294).
"""
G = 6.674*10**-11 * pq.newton * pq.meter**2 * pq.kg**-2
G_cgs = G.rescale(pq.cm**3 / (pq.g * pq.s**2))
SOLARMASS = 1.98847 * 10**30 * pq.kg
M_NS = (1.74 * SOLARMASS).rescale(pq.g)
R_NS = (10 * pq.km).rescale(pq.cm)
I_NS = (0.4 * M_NS * R_NS**2).rescale(pq.g * pq.cm**2)

"""
------------------------------------------------------------------------------------------------------------------------
---------------------
Input Parameters
---------------------
nu_initial is the initial frequency of the Neutron Star in hertz.
b is the magnetic field of the Neutron Star in Gauss.
Chi is the inclination angle between the rotation axis and the magnetic axis of the Neutron Star.
beta is a dimensionless parameter.
"""
nu_initial = 10.5e-3 * pq.hertz
b = 6e13 * pq.gauss
chi = 45 * pq.deg
beta = 0.3
m_dot_observed = 16.5  # From observed luminosity via L_x = G * M_NS * M_dot_observed / R_NS

"""
---------------------
Ranges
---------------------
t is a time range in units of seconds.
m_dot_tot is a range of the total mass accretion rate in units 10**17 g/s.
phi is a range of phase angles between 0 and 2 pi, dimensionless.
"""
dt = 0.5 * 365 * 86400
t = np.arange(0, 20. * 365 * 86400., dt)  # 400 years * 365.25 days/year * 86400 seconds/day = 12623040000 seconds
m_dot_tot = np.arange(0.1, 1.72, 0.001 * 1.72)
dphi = 0.001 * 2 * np.pi
phi = np.arange(0.0, 2 * np.pi - dphi, dphi)

"""
------------------------------------------------------------------------------------------------------------------------
---------------------
Variables (based on Input Parameters)
---------------------
mu is the magnetic moment.
mu30 is the magnetic moment in units of 10**30 G / cm**3.
"""
mu = b * R_NS**3
mu_cgs = mu.rescale(pq.gauss * pq.cm**3)
mu30 = mu_cgs / (10**30 * pq.gauss * pq.cm**3)

"""
---------------------
Functions
---------------------
"""


def get_omega_0(nu_val):
    """
    :param nu_val: A frequency in units hertz.
    :return: Angular velocity in units hertz.
    """
    omega_0_val = (2 * np.pi * nu_val)
    return omega_0_val


def get_r_co(omega_0_val):
    """
    :param omega_0_val: Angular velocity in units hertz.
    :return: Corotation radius: where the angular velocity of the disk is the same as the NS.
    """
    r_co_val = (G_cgs * M_NS / omega_0_val**2)**(1/3)
    return r_co_val


def get_r_inf(r_co_val):
    """
    :param r_co_val: Corotation radius.
    :return: Radius beyond which matter is only ejected anymore.
    """
    r_inf_val = ((beta + np.sqrt(2)) / (1 + beta)) ** (2 / 3) * r_co_val
    return r_inf_val


def get_r_m(phi_var, m_dot_tot_var):
    """
    :param phi_var: Phase angle between 0 and 2 pi. It is dimensionless.
    :param m_dot_tot_var: Total mass accretion rate in units of 10**17 g/s.
    :return: Magnetospheric radius in units of cm, see Perna 2006 (3).
    """
    r_m_val = \
        3.24 * 10**8 * mu30**(4/7) * 1.4**(-1/7) * m_dot_tot_var**(-2/7) \
        * (1 + 3 * (np.sin(chi.rescale(pq.rad)) * np.sin(phi_var + dphi / 2.)) ** 2)**(2/7) * pq.cm
    return r_m_val


def get_omega_k(get_r_m_val):
    """
    :param get_r_m_val: Magnetospheric radius in units cm.
    :return: Keplerian angular velocity at the magnetospheric radius in units hertz, see Wang 1987 (2).
    """
    get_omega_k_val = ((G_cgs * M_NS / get_r_m_val**3)**(1/2))
    return get_omega_k_val


def get_nu_dot(m_dot_tot_var, get_r_m_var, omega_k_var, a_const_var, phi_val, omega_0_val):
    """
    :param m_dot_tot_var: Total mass accretion rate in units of 10**17 g/s.
    :param get_r_m_var: Magnetospheric radius in units cm.
    :param omega_k_var: Keplerian angular velocity at the magnetospheric radius in units 1/s.
    :param a_const_var: Dimensionless parameter, either 0 or 1.
    :param phi_val: Phase angle between 0 and 2 pi. It is dimensionless.
    :param omega_0_val: Angular velocity in units hertz.
    :return: Change in frequency in units hertz/s.
    """
    nu_dot_val = \
        m_dot_tot_var * 10**17 * pq.g / pq.s / (2*np.pi * I_NS) * (G_cgs * M_NS * get_r_m_var)**(1/2) \
        * (1 - a_const_var * (1 + (1 + beta) * (omega_0_val / omega_k_var - 1))) \
        / len(phi_val)
    return nu_dot_val


def luminosity_disk(get_r_m_val, m_dot_star_val):
    """
    :param get_r_m_val: Magnetospheric radius in units cm.
    :param m_dot_star_val: Mass accretion rate from the companion star in units of g/s.
    :return: Contribution of the disk to the luminosity.
    """
    luminosity_disk_val = G_cgs * M_NS * m_dot_star_val / (2 * get_r_m_val)
    return luminosity_disk_val


def luminosity_acc(get_r_m_val, m_dot_acc_val):
    """
    :param get_r_m_val: Magnetospheric radius in units cm.
    :param m_dot_acc_val: Fraction of m_dot_tot that accretes matter in units of 10**17 g/s.
    :return: Contribution of the accreted matter to the luminosity in erg/s.
    """
    luminosity_acc_val = \
        (G_cgs * M_NS * ((1/R_NS) - (1/get_r_m_val)) + ((omega_0**2/2) * (get_r_m_val**2 - R_NS**2))) * \
        m_dot_acc_val
    return luminosity_acc_val


def luminosity_rec(omega_k_var_val, get_r_m_val, m_dot_rec_val):
    """
    :param omega_k_var_val: Keplerian angular velocity at the magnetospheric radius in units 1/s.
    :param get_r_m_val: Magnetospheric radius in units cm.
    :param m_dot_rec_val: Fraction of m_dot_tot that recycles matter in units of 10**17 g/s.
    :return: Contribution of the recycled matter to the luminosity in erg/s.
    """
    luminosity_rec_val = \
        (((omega_k_var_val * get_r_m_val * (1 - ((1+beta)*(1 - (omega_0.rescale(pq.s**-1) / omega_k_var_val)))))**2)/2 -
         (G_cgs * M_NS / (2*get_r_m_val))) * m_dot_rec_val
    return luminosity_rec_val


def luminosity_bl_1(m_dot_tot_var, get_r_m_val, omega_k_var_val, phi_val):
    """
    :param m_dot_tot_var: Total mass accretion rate in units of 10**17 g/s.
    :param get_r_m_val: Magnetospheric radius in units cm.
    :param omega_k_var_val: Keplerian angular velocity at the magnetospheric radius in units 1/s.
    :param phi_val: Phase angle between 0 and 2 pi. It is dimensionless.
    :return: Contribution of the boundary layer to the luminosity in erg/s.
    """
    luminosity_bl_1_val = m_dot_tot_var / 2 * get_r_m_val**2 * (omega_k_var_val**2 - omega_0**2) / len(phi_val)
    return luminosity_bl_1_val


def luminosity_bl_2(m_dot_tot_var, get_r_m_val, omega_k_var_val, phi_val):
    """
    :param m_dot_tot_var: Total mass accretion rate in units of 10**17 g/s.
    :param get_r_m_val: Magnetospheric radius in units cm.
    :param omega_k_var_val: Keplerian angular velocity at the magnetospheric radius in units 1/s.
    :param phi_val: Phase angle between 0 and 2 pi. It is dimensionless.
    :return: Contribution of the boundary layer to the luminosity in erg/s.
    """
    luminosity_bl_2_val = \
        m_dot_tot_var / 2 * (1 - beta) * get_r_m_val**2 * (omega_0**2 - omega_k_var_val**2) / len(phi_val)
    return luminosity_bl_2_val


"""
---------------------
Calculations
---------------------
"""
"""
nu_list is a list of frequencies for each time interval.
nu_dot_list is a list of the frequency derivative for each time interval.
lum_tot is a list of the luminosity for each time interval.
branch_up = True indicates whether or not we are initially on the spin-up branch.
"""

frequency = nu_initial / pq.hertz
nu_list = [0 for i in range(len(t))]
nu_dot_list = []
lum_tot = []
branch_up = False

""" The outer most loop is to iterate over time. At the end of the loop there will be three lists comprising the 
time dependent frequency (nu_list), frequency derivative (nu_dot_list) and luminosity (lum_dot)."""
for q in t:
    """ For each time step there will be a new frequency and therefore a new omega_0, R_co and R_inf have to be
    determined."""
    omega_0 = get_omega_0(frequency * pq.hertz)
    R_co = get_r_co(omega_0)
    R_inf = get_r_inf(R_co)

    m_dot_acc_list = []
    m_dot_eje_list = []
    m_dot_rec_list = []
    nu_dot_1_min = []
    nu_dot_2_min = []
    """ The following loop is to determine the different contributions of the accreted, recycled and ejected matter
    of the total mass accretion rate. Also, the nu_dot values for the range of m_dot_tot is determined to later
    see if the system is on a spin-up or spin-down branch."""
    for i in m_dot_tot:
        m_dot_acc = 0
        m_dot_eje = 0
        m_dot_rec = 0
        nu_dot_1_val_min = 0
        nu_dot_2_val_min = 0

        R_M_m_dot = get_r_m(phi, i)
        omega_k = get_omega_k(R_M_m_dot)

        # array of elements where R_M is less than R_co:
        el_rm_lt_rco = R_M_m_dot.compress((R_M_m_dot < R_co.rescale(pq.cm)).flat)
        # array of elements where R_M is greater than R_co:
        el_rm_gt_rinf = R_M_m_dot.compress((R_M_m_dot >= R_inf.rescale(pq.cm)).flat)
        # array of elements where R_M is between R_co and R_inf:
        el_rm_bt_rco_rinf = \
            R_M_m_dot.compress(((R_M_m_dot >= R_co.rescale(pq.cm)) & (R_M_m_dot < R_inf.rescale(pq.cm))).flat)

        m_dot_acc_list.append(i / len(phi) * len(el_rm_lt_rco))
        m_dot_eje_list.append(i / len(phi) * len(el_rm_gt_rinf))
        m_dot_rec_list.append(i / len(phi) * len(el_rm_bt_rco_rinf))

        nu_dot_1_min_arr = get_nu_dot(i, R_M_m_dot, omega_k, 0, phi, omega_0) * pq.s / pq.hertz
        nu_dot_2_min_arr = get_nu_dot(i, R_M_m_dot, omega_k, 1, phi, omega_0) * pq.s / pq.hertz

        nu_dot_1_min.append(sum(nu_dot_1_min_arr.compress((R_M_m_dot < R_co.rescale(pq.cm)).flat)))
        nu_dot_2_min.append(sum(nu_dot_2_min_arr.compress((R_M_m_dot >= R_co.rescale(pq.cm)).flat)))

    # Get the index of the nu_dot value of the minimum so we can differ between spin-up and spin-down branch:
    nu_dot_min = [sum(x) for x in zip(nu_dot_1_min, nu_dot_2_min)]
    nu_dot_min = list(map(float, nu_dot_min))
    nu_dot_list_test = np.log10(np.abs(nu_dot_min))
    nu_dot_min = np.log10(np.abs(nu_dot_min))
    nu_dot_min = nu_dot_min.tolist().index(min(nu_dot_min))

    """ The value m_dot_star is defined to be the sum of the accreted and ejected mass accretion rates.
    m_dot_acc_list and m_dot_rec_list are later used to determine different contributions to the luminosity."""
    m_dot_star = [sum(x) for x in zip(m_dot_acc_list, m_dot_eje_list)]
    m_dot_acc_list = list(map(float, m_dot_acc_list))
    m_dot_rec_list = list(map(float, m_dot_rec_list))

    m_dot_tot_log = np.log10(m_dot_tot)
    m_dot_star_log = np.log10(m_dot_star)

    # Get the m_dot_tot value corresponding to the minimum nu_dot value (spin-up/spin-down branch):
    m_dot_tot_min = m_dot_tot_log[nu_dot_min] + 17

    """ All intersections of the m_dot_star curve with the constant value m_dot_observed are determined and stored
    in a list."""
    m_dot_tot_sol_list = []
    # m_dot_tot_sol_list = \
    #    [w[0] for w in zip(m_dot_tot_log + 17, m_dot_star_log + 17) if abs(w[1] - m_dot_observed) < 0.005]

    for x in range(0, len(m_dot_star_log)-1):
        if m_dot_star_log[x] + 17 < m_dot_observed < m_dot_star_log[x+1] + 17:
            m_dot_tot_sol_list.append(m_dot_tot_log[x+1] + 17)

    """ Depending on where the system was in the past (spin-up/spin-down) the new m_dot_tot_sol value is determined:
    If it was on the spin-up branch and still is, the last intersection is chosen. If it was on the spin-up branch
    but now jumped to the spin-down branch, the first intersection is chosen and the counter branch_up is set to False.
    Starting on the spin-down branch changes the intersections correspondingly."""
    if branch_up:
        if m_dot_tot_sol_list[-1] > m_dot_tot_min:
            m_dot_tot_sol = m_dot_tot_sol_list[-1]
        else:
            m_dot_tot_sol = m_dot_tot_sol_list[0]
            branch_up = False
    else:
        if m_dot_tot_sol_list[0] < m_dot_tot_min:
            m_dot_tot_sol = m_dot_tot_sol_list[0]
        else:
            m_dot_tot_sol = m_dot_tot_sol_list[-1]
            branch_up = True

    R_M = get_r_m(phi, (10**m_dot_tot_sol / 10**17))
    omega_k = get_omega_k(R_M)

    """ In the following, the contributions of the luminosity are determined and summed up."""
    lum_disk = sum(luminosity_disk(R_M, 10**16.3 / 10**17 * pq.s**3 / (pq.g * pq.cm**2))*pq.g/pq.s / len(phi))
    m_dot_lum = (10**m_dot_tot_sol / 10**17 / len(phi))
    lum_acc_arr = luminosity_acc(R_M, m_dot_lum * pq.g/pq.s) * pq.s**3 / (pq.g * pq.cm**2)
    lum_rec_arr = luminosity_rec(omega_k, R_M, m_dot_lum * pq.g/pq.s) * pq.s**3 / (pq.g * pq.cm**2)
    lum_bl_1_arr = luminosity_bl_1(10**m_dot_tot_sol / 10**17 * pq.g/pq.s, R_M, omega_k, phi)*pq.s**3 / (pq.g*pq.cm**2)
    lum_bl_2_arr = luminosity_bl_2(10**m_dot_tot_sol / 10**17 * pq.g/pq.s, R_M, omega_k, phi)*pq.s**3 / (pq.g*pq.cm**2)
    lum_acc = sum(lum_acc_arr.compress((R_M < R_co.rescale(pq.cm)).flat))
    lum_rec = sum(lum_rec_arr.compress((R_inf.rescale(pq.cm) > R_M) & (R_M > R_co.rescale(pq.cm)).flat))
    lum_bl_1 = sum(lum_bl_1_arr.compress((R_M < R_co.rescale(pq.cm)).flat))
    lum_bl_2 = sum(lum_bl_2_arr.compress((R_M >= R_co.rescale(pq.cm)).flat))

    lum_tot_val = lum_disk + lum_acc + lum_rec + lum_bl_1 + lum_bl_2
    lum_tot_val = lum_tot_val
    lum_tot.append(lum_tot_val)

    """ The frequency derivative nu_dot at m_dot_tot_sol is determined and stored in a list."""
    nu_dot_1_arr = get_nu_dot(10**m_dot_tot_sol / 10**17, R_M, omega_k, 0, phi, omega_0) * pq.s / pq.hertz
    nu_dot_2_arr = get_nu_dot(10**m_dot_tot_sol / 10**17, R_M, omega_k, 1, phi, omega_0) * pq.s / pq.hertz

    nu_dot_1 = sum(nu_dot_1_arr.compress((R_M < R_co.rescale(pq.cm)).flat))
    nu_dot_2 = sum(nu_dot_2_arr.compress((R_M >= R_co.rescale(pq.cm)).flat))
    nu_dot_sol = nu_dot_1 + nu_dot_2
    nu_dot_list.append(nu_dot_sol)

    """ With the frequency derivative the new frequency can be calculated and is stored in a list."""
    frequency = frequency + (nu_dot_sol * dt)
    nu_list[int(q/dt)] = frequency

    """ For each time step, a plot is created including the frequency derivative over the total mass accretion rate 
    and the mass accretion rate from the star over the total mass accretion rate."""
    f, axarr = plt.subplots(2, sharex='col')
    axarr[0].plot(m_dot_tot_log + 17, nu_dot_list_test, linewidth=0.7, c='k')
    axarr[1].plot(m_dot_tot_log + 17, m_dot_star_log + 17, linewidth=0.7, c='k')

    axarr[1].set_xlabel(r"$\log[\dot{M}_{tot}$ (g/s)]")
    axarr[0].set_ylabel(r"$\log[\dot{\nu}$] (Hz/s)")
    axarr[1].set_ylabel(r"$\log[\dot{M}$ (g/s)]")
    plt.setp(axarr[0].get_yticklabels()[0], visible=False)
    f.subplots_adjust(hspace=0.0, wspace=0.3)
    axarr[0].tick_params(top=True, right=True, which='both', direction='in')
    axarr[1].tick_params(top=True, right=True, which='both', direction='in')
    axarr[0].minorticks_on()
    axarr[1].minorticks_on()
    plt.savefig('Figures/GIF/{p}_b={b}e12G_chi={chi}_beta={beta}.pdf'.format(
        p=int(q/dt), b=int(b/1e12), chi=int(chi), beta=int(beta*10)), format='pdf', orientation='landscape')
    plt.close()
"""
---------------------
Plot
---------------------
The final time dependent plots are created:
- Frequency over time
- Frequency derivative over time
- Luminosity over time
"""

f, axarr = plt.subplots(3, sharex='col')
axarr[0].plot(t/365/86400., nu_list, linewidth=0.7, c='k')
axarr[1].plot(t/365/86400., np.log10(np.abs(nu_dot_list)), linewidth=0.7, c='k')
axarr[2].plot(t/365/86400., np.log10(lum_tot) + 17, linewidth=0.7, c='k')

axarr[2].set_xlabel('t [yr]')
axarr[0].set_ylabel('Frequency [mHz]')
axarr[1].set_ylabel(r'$\log|\dot{\nu}$| [Hz/s]')
axarr[2].set_ylabel(r'$\log$ L [erg/s]')
plt.setp(axarr[0].get_yticklabels()[0], visible=False)
plt.setp(axarr[2].get_yticklabels()[-1], visible=False)
f.subplots_adjust(hspace=0.0, wspace=0.3)
axarr[0].tick_params(top=True, right=True, which='both', direction='in')
axarr[1].tick_params(top=True, right=True, which='both', direction='in')
axarr[2].tick_params(top=True, right=True, which='both', direction='in')
axarr[0].minorticks_on()
axarr[1].minorticks_on()
axarr[2].minorticks_on()

plt.savefig('Figures/Time_evolution/b={b}e12G_chi={chi}_beta={beta}.pdf'.format(
    b=int(b/1e12), chi=int(chi), beta=int(beta*10)), format='pdf', orientation='landscape')
plt.close()
