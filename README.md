Inclined Rotator

This program is based on Perna, R., Bozzo, E., & Stella, L. 2006, ApJ, 639, 363.

In the spirit of Reproducible Research, the present program includes the 
program (written in Python) that is needed to reproduce the results of Chapter
4 of my bachelor thesis "Exploring Time Variability Properties of X-ray Pulsars 
through Accretion Torque Models".

By compiling the program, all Figures in the "Figures" folder will be 
reproduced. The Figures from Chapter 4 can then be created by changing the 
parameters as described in the thesis.

Please contact me under inga.saathoff@gmail.com with any questions or problems.